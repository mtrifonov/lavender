import logo from './logo.svg';
import './App.css';
import Modes from './components/Modes';
import Doc from './components/Doc';

function App() {
  return (
    <div className="App">
      <Modes></Modes>
      <Doc></Doc>
    </div>
  );
}

export default App;
