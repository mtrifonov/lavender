import Box from './Box';
import Tree from './Tree';
import { useRef, useState, useLayoutEffect } from 'react';
import dummy_data from './dummy_data';
import treeBack from '../utilities/treeBack';

function Doc() {

    const store = useRef({
        content: treeBack(dummy_data),
        active: "undefined",
        counter: 0
    });


    const insertAt = (array,index,item) => {
        let firstSection = array.slice(0,index);
        let lastSection = array.slice(index,array.length);
        return [].concat(firstSection,item,lastSection);
    } 

    const [state, setState] = useState({
        action: "initial",
        count: 0,
    });

    const insertRender = (data,store) => {
        if (state.action != "initial") {
            if (store.target.parentId === data.id) {
                if (store.target.id != store.active.id) {
                    let target = {
                        type: "target",
                        id: "target"
                    }
                    let temp = insertAt(data.contains,store.target.index,target);
                    return temp.map(obj => renderBoxTree(store,obj));
                } 
            }
        }
        return data.contains.map(obj => renderBoxTree(store,obj))
    }

    const renderBoxTree = (store,data, active=false) => {
        if (data.type === 'target') {
            return <Box 
            type={"target"}
            store={store}
            self={data}
            key={data.id}
            docState={state}
            setDocState={setState}
            ></Box>
        } else if (data.type === 'content') {
            return <Box 
            type={active ? "pointer" : data.active ? "phantom" : "default"}
            store={store}
            self={data}
            key={data.id}
            docState={state}
            setDocState={setState}
            >{data.content}</Box>
        } else if (data.type === 'container') {
            return <Box 
            type={active ? "pointer" : data.active ? "phantom" : "default"}
            store={store}
            self={data}
            key={data.id}
            docState={state}
            setDocState={setState}
            >{
                insertRender(data,store)
            }</Box>
        }
    }
        
    if (state.action === "initial") {
        return (
            <div>
                {renderBoxTree(store.current,store.current.content)}
            </div>
          );
    } else if (state.action === "startDrag") {
        return (
            <div>
                {renderBoxTree(store.current,store.current.content)}
                <div 
                style={{
                    position: "absolute",
                    top: "0px",
                    left: "0px",
                    width: "100vw"
                }}>
                    {renderBoxTree(store.current,store.current.active,true)}
                </div>
            </div>
          );
    } else if (state.action === "dragChange") {
        return (
            <div>
                {renderBoxTree(store.current,store.current.content)}
                <div 
                style={{
                    position: "absolute",
                    top: "0px",
                    left: "0px",
                    width: "100vw"
                }}>
                    {renderBoxTree(store.current,store.current.active,true)}
                </div>
            </div>
          );
    }

  }
  
  export default Doc;
  