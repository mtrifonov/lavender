import styles from './styles/Box.module.css';
import { useDrag } from 'react-use-gesture';
import { animated, useSpring } from 'react-spring';
import { fireEvent, removeEvent, handleEvent } from './GlobalAnimationState';
import { useLayoutEffect, useRef, useEffect } from 'react';

function TargetBox(props) {
  return (<div 
    className={`${styles.main} ${styles.target}`}
  >
  </div>) 
}
  
  export default TargetBox;
  