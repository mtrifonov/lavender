import styles from './styles/Box.module.css';



function PhantomBox(props) {
  return (<div 
    style={{height:props.self.height}} 
    className={`${styles.phantom}`}
  ></div>) 
}

export default PhantomBox;
  