
const dummy_data = {
    type: "container",
    id: "0-0",
    root: true,
    contains: [
        {
            type: "container",
            id: "1-0",
            contains: [
                {
                    type: "content",
                    id: "2-0",
                    content: "box 1"
                },
                {
                    type: "content",
                    id: "2-1",
                    content: "box 2"
                },
                {
                    type: "content",
                    id: "2-2",
                    content: "box 3"
                },
                {
                    type: "container",
                    id: "2-3",
                    contains: [
                        {
                            type: "content",
                            id: "3-0",
                            content: "box 1"
                        },
                        {
                            type: "content",
                            id: "3-1",
                            content: "box 2"
                        }
                    ]
                },
            ]
        },
        {
            type: "content",
            id: "1-1",
            content: "box 3"
        }
    ]
}

export default dummy_data;