import styles from './styles/Box.module.css';

function DefaultBox(props) {
  return (<div 
    className={`${props.self.root ? styles.root : `${styles.main}`}`}
  >
    {props.children}
  </div>) 
}

export default DefaultBox;
  