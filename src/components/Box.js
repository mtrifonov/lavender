import styles from './styles/Box.module.css';
import { useDrag } from '@use-gesture/react';
import { animated, useSpring } from 'react-spring';
import { fireEvent, removeEvent, handleEvent } from './GlobalAnimationState';
import { useLayoutEffect, useRef, useEffect } from 'react';
import PhantomBox from './PhantomBox';
import DefaultBox from './DefaultBox';
import PointerBox from './PointerBox';
import TargetBox from './TargetBox';
import { findRenderedComponentWithType } from 'react-dom/cjs/react-dom-test-utils.production.min';

function Box(props) {


    const ref = useRef();

    const gesture = useDrag(({movement:[x,y], xy:[ix,iy], event,last,first}) => {
      event.stopPropagation();
      if (last) {      

      };
      if (first) {
        props.self.active = true;
        props.self.offsetTop = props.self.top;
        props.self.offsetLeft = props.self.left;
        props.store.active = props.self;
        props.store.target = {
          id: props.self.id,
        }
        props.setDocState({action:"startDrag",count:props.docState.count+1});
      }
      fireEvent([x,y],[ix,iy]);
    });

    /*
    useEffect(() => {
      console.log(props.self.id);
    },[]);
    */
    useEffect(() => {
      console.log(props.self.id);
    },[]);

    // Measurements
    useEffect(() => {
      if (props.type != 'pointer') {
        const measurements = ref.current.getBoundingClientRect();
        props.self.height = measurements.height;
        props.self.width = measurements.width;
        props.self.top = measurements.top;
        props.self.left = measurements.left;
        props.self.ypos = (measurements.top + measurements.bottom) / 2; 
      }
    });

    function renderType() {
      switch (props.type) {
        case "pointer":
          return (<PointerBox
            self={props.self}
            children={props.children}
            store={props.store}
            setDocState={props.setDocState}
            docState={props.docState}

          ></PointerBox>)
        case "phantom":
          return (<PhantomBox
            self={props.self}
            children={props.children}
          ></PhantomBox>)
        case "target":
          return (<TargetBox
            self={props.self}
            children={props.children}

          ></TargetBox>)
        case "default":
          return (<DefaultBox
            self={props.self}
            children={props.children}

          ></DefaultBox>)
        default:
          return (<DefaultBox
            self={props.self}
            children={props.children}
          ></DefaultBox>)
  
      }
    }


    return (<div
    className={`${props.type==='pointer' ? null : styles.offset}`}
    ref={ref}
    {...gesture()} 
    >
      {renderType()}
    </div>)

  }
  
  export default Box;
  