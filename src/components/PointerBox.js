import styles from './styles/Box.module.css';
import { useDrag } from 'react-use-gesture';
import { animated, useSpring } from 'react-spring';
import { fireEvent, removeEvent, handleEvent } from './GlobalAnimationState';
import { useLayoutEffect, useRef, useEffect } from 'react';

function PointerBox(props) {

    const [spring, setSpring] = useSpring(() => ({
      transform: `translate(0px,0px)`,
    }));

    useEffect(() => {
      function traverseTree(tree,x,y,obj) {
        if (tree.type === "container") {
          obj[tree.id] = {};
          let curObj = obj[tree.id];
          tree.contains.forEach((item,index) => {
            curObj[index] = {
              distance: Math.sqrt(Math.pow(item.top - y,2)+Math.pow(item.left -x,2)),
              id: item.id,
              ref: item,
            }
            traverseTree(item,x,y,obj);
          })
        }
      }
      function findMin(obj) {
        const output = {
          distance: Number.POSITIVE_INFINITY,
        }
        for (const list in obj) {
          for (const item in obj[list]) {
            if (obj[list][item].distance < output.distance) {
              output.index = item;
              output.distance = obj[list][item].distance;
              output.id = obj[list][item].id;
              output.parentId = list;
            }
          }
        }
        return output;
      }
      function findClosestPoint(xy) {
        let [x,y] = xy;
        //console.log(props.self.top);
        x = x + props.self.offsetLeft;
        y = y + props.self.offsetTop;

        let obj = {}
        traverseTree(props.store.content,x,y,obj);
        let min = findMin(obj);
        //console.log(obj["1-0"]);
        return min;
      }
      function func(e) {
        let [x,y] = e.detail.xy;
        setSpring.start({
          transform: `translate(${x}px,${y}px)`,
          immediate: true,
        })
        let obj = findClosestPoint(e.detail.xy);
        //console.log(props.self.id,obj.id);
        if (obj.id === props.self.id || obj.id === 'target') {
          console.log("do nothing");
          return
        } else if (obj.id != props.store.target.id) {
          console.log("change needed")
          props.store.target = obj;
          props.setDocState({action:"dragChange",count:props.docState.count+1});
        } 
        /* 
        if (obj.id === props.self.id || obj.id === 'target') {
          console.log("do nothing");
        } else console.log(obj);
        let target = props.store.target;*/
      }
      handleEvent(func);
      return removeEvent(func);
    },[]);


    return (<animated.div 
      style={{...spring,
      ...{top:props.self.top,left:props.self.left}}} 
      className={`${styles.main} ${styles.pointer}`}
    >
      {props.children}
    </animated.div>)

}
  
  export default PointerBox;
  