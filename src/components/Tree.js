import { useRef } from 'react';
import styles from './styles/Tree.module.css';

function Tree(props) {

    const drawVertLines = (data,level) => {
        if (data.type === 'container') {
            const first = data.contains[0].ypos;
            const last = data.contains[data.contains.length -1].ypos;
            const xpos = `${level * 60 - 10}px`;
            const el = <line x1={xpos} y1={first} x2={xpos} y2={last} style={{stroke:"rgb(255,0,0)",strokeWidth:2}}/>;
            return [].concat(el,...data.contains.map(obj => drawVertLines(obj,level+1)));
        } else return [];
    }

    const drawHorLines = (data,level) => {
        if (data.type === 'container') {
            const ypos = data.ypos;
            const xpos1 = `${level * 60 - 10}px`;
            const xpos2 = `${(level+1) * 60 - 10}px`;
            const el = <line x1={xpos1} y1={ypos} x2={xpos2} y2={ypos} style={{stroke:"rgb(255,0,0)",strokeWidth:2}}/>;
            return [].concat(el,...data.contains.map(obj => drawHorLines(obj,level+1)));
        } else return [];
    }


    return (
        <svg className={styles.main}>
            {drawVertLines(props.store.content,1)}
            
        </svg>
    );
  }
  
  export default Tree;
  