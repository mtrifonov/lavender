
export const fireEvent = (xy,ixy) => {
    const event = new CustomEvent('globAnimState', {detail: {
        xy: xy,
        ixy: ixy
    }});
    window.dispatchEvent(event);
}

export const handleEvent = (func) => {
    return window.addEventListener('globAnimState', func);
}

export const removeEvent = (func) => {
    return () => window.removeEventListener('globAnimState', func);
}
