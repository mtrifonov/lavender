const treeBack = (tree) => {
    if (tree.type === 'container') {
        tree.contains = tree.contains.map(obj => {
            obj.parent = tree;
            return treeBack(obj);
        })
        return tree;
    } else if(tree.type === 'content' || tree.type === 'target') {
        return tree;
    }
}

export default treeBack;